;
(function ($, window, undefined) {
  'use strict';
  // code using $ as alias to jQuery
  $(function () {
    // more code using $ as alias to jQuery

    if (typeof BORORDA === "undefined") {
      var BORORDA = {};
    } else {
      console.log('app init error: name conflict')
    };

    BORORDA.utils = {
      checkAviablePlugin: function (pluginName) {
        if ($.fn[pluginName]) {
          return true;
        } else {
          console.log('plugin is unaviable ' + pluginName)
          return false;
        }
      }
    };

    BORORDA.materialize = function () {
      $('.dropdown-button').dropdown();
      $(".button-collapse").sideNav();
      $('.modal').modal();
    };

    BORORDA.pusher = function () {
      $('.header__trigger').on('click', function () {
        $('.wrapper').toggleClass('menu__open')
      });
    };

    BORORDA.paralax = function () {
      var controller = new ScrollMagic.Controller();
      // Section Home
      var hero = $('.hero').get(0);
      var sceneHome = new ScrollMagic.Scene({
        triggerHook: 'onLeave',
        triggerElement: hero,
        duration: '100%'
      }).on("progress", function (event) {
        $('.paralax').css('transform', 'translateY(' + event.progress * 20 + '%)');
      });

      var $sections = $('.parralaxBlock');

      $sections.each(function (index, item) {
        var sceneParalax = new ScrollMagic.Scene({
          triggerHook: 'onEnter',
          triggerElement: item,
          duration: '200%'
        }).on("progress", function (event) {
          $(item).find('.parralaxBlock__img').css('transform', 'translateY(' + (event.progress * 20 - 10) + '%)');
        });
        controller.addScene(sceneParalax);
      });

      //Controller
      controller.addScene(sceneHome);
    };

    BORORDA.materialize();
    BORORDA.paralax();
    BORORDA.pusher();

  });

})(jQuery, window, undefined);