module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);
  grunt.loadNpmTasks('grunt-svg2string');

  var theme_name = 'vidzo';
  // var base_theme_path = '../zurb_foundation';

  var global_vars = {
    theme_name: theme_name,
    theme_css: 'css',
    theme_scss: 'scss',
    // base_theme_path: base_theme_path
  };

  grunt.initConfig({
    global_vars: global_vars,
    pkg: grunt.file.readJSON('package.json'),

    // sass: {
    //   dist: {
    //     options: {
    //       outputStyle: 'expanded',
    //       includePaths: ['scss/']
    //     },
    //     files: {
    //       '../css/app.css': 'scss/app.scss'
    //     }
    //   }
    // },
    svg2string: {
      elements: {
        files: {
          'build/js/svg.js': [
            'src/svg/*.svg'
          ]
        }
      }
    }

    // jshint: {
    //   options: {
    //     jshintrc: '.jshintrc'
    //   },
    //   all: [
    //     'Gruntfile.js',
    //     jsApp
    //   ]
    // },

    // uglify: {
    //   options: {
    //     sourceMap: false
    //   },
    //   dist: {
    //     files: {
    //       'js/libs.min.js': [jsLibs],
    //       'js/foundation.min.js': [jsFoundation],
    //       'js/app.min.js': [jsApp]
    //   }
    //   }
    // },

    // watch: {
    //   grunt: { files: ['Gruntfile.js'] },

    //   sass: {
    //     files: 'scss/**/*.scss',
    //     tasks: ['sass'],
    //     options: {
    //       livereload: true
    //     }
    //   }

      // js: {
      //   files: [
      //     jsLibs,
      //     jsFoundation,
      //     '<%= jshint.all %>'
      //   ],
      //   tasks: ['jshint', 'uglify']
      // }
  });

  grunt.registerTask('default', ['svg2string']);
};